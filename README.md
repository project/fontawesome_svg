Introduction
------------
The Font Awesome SVG module allows you to display the famous Font Awesome icons
as inline SVG images. There is much debate about the merits of icon fonts versus
using SVG vector graphics. For an introduction on the topic, see e.g. 
https://css-tricks.com/icon-fonts-vs-svg

To submit bug reports and feature suggestions, or to track changes:
https://drupal.org/project/issues/fontawesome_svg


Requirements
------------
This module requires:

 * [Icon API](https://drupal.org/project/icon)
 * [Libraries API](https://www.drupal.org/project/libraries)
 * [Font Awesome SVG PNG](https://github.com/encharm/Font-Awesome-SVG-PNG)


Installation
------------
 
 * Install the [Icon API](https://drupal.org/project/icon) module.
 * Install as you would normally install a contributed Drupal module. See
   [Installing modules (Drupal 
   7)](https://drupal.org/documentation/install/modules-themes/modules-7)
   for further information.
 * The Font Awesome SVG PNG distribution needs to be downloaded from its 
   [project page on GitHub](https://github.com/encharm/Font-Awesome-SVG-PNG). 
   Extract the archive in the sites/all/libraries directory in a directory 
   called fontawesome-svg-png. There is an example drush make file in the 
   project that you can either rename, or use its contents to use in your own 
   makefile.


Configuration
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will expose an icon bundle to the Icon API module, which 
exposes them to be used in an icon field, with menus, etc. using add-on modules. 

Note that the module caches the content of the individual SVG files. Should 
there be any changes to the icons, make sure you clear the Drupal cache.

Be prepared for having to apply styling to the icons through your theme; 
without any styling, the icons will display at 30x30 pixels in black. You 
can apply styling through regular CSS, typically width, height and fill (for the 
color) attributes. 


Wishlist
--------
The current implementation of this module is fairly basic, all it does is add 
the icons inline. Possible improvements:

 * Drop-in replacement for [Font 
   Awesome](https://drupal.org/project/fontawesome) module, so the markup that
   module allows is also accepted.
 * Optionally add the SVG as data-url background images in CSS instead of inline 
   XML.
 * Implement the reference technique as described in 
   https://css-tricks.com/svg-symbol-good-choice-icons/, useful when the same
   icon is repeated on the same page (should only add the icons used on the 
   current page).
 * Possibly do something with the PNG component of the supporting distribution?


Troubleshooting
---------------
 * If you enabled the module but you don't see it offering any icons, check 
   whether you download the Font Awesome SVG distribution (see section 
   Installation).
 * If the icons don't display, check whether the individual SVG files can be read 
   by Drupal/PHP.


Maintainers
-----------
Current maintainer:

 * Eelke Blok (eelkeblok) - https://www.drupal.org/u/eelkeblok

This project has been sponsored by:

 * Dutch Open Projects  
   DOP is a leading Drupal implementer in the Netherlands and provides complete 
   solutions for their customers, from functional design including wireframing 
   to a working Drupal site including maintenance, training and hosting. 
   Visit http:/dop.nu (mostly Dutch) for more information.
